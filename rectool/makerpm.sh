#!/bin/sh
# Create RPM files for rec10-rectool


RPMBUILD=`mktemp -d $HOME/rpmbuild.XXXXXX`
#RPMMACRO=`mktemp    $HOME/.rpmmacros.XXXXXX`
echo "Build directory $RPMBUILD"
mkdir -p $RPMBUILD/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
cp {rectool.pl,rec10.conf} $RPMBUILD/SOURCES
cp rectool.spec $RPMBUILD/SPECS
echo "%_topdir $RPMBUILD" > ~/.rpmmacros
rpmbuild -ba "$RPMBUILD/SPECS/rectool.spec"
cp $RPMBUILD/RPMS/noarch/* .
cp $RPMBUILD/SRPMS/* .

#sleep 5
#read -p "Press Enter key to exit."
echo 'Remove build directory'
rm -r $RPMBUILD
#rm $RPMMACRO

