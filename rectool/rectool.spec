%define		gitrev	%(git rev-list HEAD | wc | awk '{print $1}')

Name:		rec10-rectool
Version:	0.0.1
Release:	git%{gitrev}%{?dist}
Summary:	rec10 web interface

Group:		Utilities
License:	Unknown
URL:		http://www.rec10.org/
Source0:	rectool.pl
Source1:	rec10.conf
BuildArch:	noarch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	make
Requires:	perl-Algorithm-Diff perl-Archive-Zip perl-CGI perl-Config-Simple perl-Data-Dumper-Concise
Requires:	perl-Date-Simple perl-DateTime perl-DBI perl-File-Slurp perl-Sort-Naturally perl-SVG
Requires:	perl-Time-Piece perl-Tie-IxHash perl-List-Compare perl-XML-Atom perl-XML-Generator-DBI perl-XML-SAX-Writer perl-XML-TreePP

%description
rectool-0.0.1

%prep
rm -rf $RPM_BUILD_ROOT

%build


%install
#rm -rf $RPM_BUILD_ROOT
%__mkdir -p $RPM_BUILD_ROOT/usr/share/rec10/rectool/
%__mkdir -p $RPM_BUILD_ROOT/etc/httpd/conf.d/
%__mkdir -p $RPM_BUILD_ROOT/var/www/html/rec10/
%__cp $RPM_SOURCE_DIR/rectool.pl $RPM_BUILD_ROOT/usr/share/rec10/rectool/
%__cp $RPM_SOURCE_DIR/rec10.conf $RPM_BUILD_ROOT/etc/httpd/conf.d/
ln -sf /usr/share/rec10/rectool/rectool.pl $RPM_BUILD_ROOT/var/www/html/rec10/

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc
%{_datadir}/rec10/rectool/rectool.pl
/etc/httpd/conf.d/rec10.conf
/var/www/html/rec10/rectool.pl


%changelog

