/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;

/**
 *
 * @author Administrator
 */
public class PIDs {
    public int Program_Table;
    public int[] PIDs;
    public int PMT_PID;
    public int PCR_PID;
}
