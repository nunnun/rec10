// -*- tab-width:4 -*-

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bit.h"

// 2-STD-B10v4_6.pdf p89 (101/399)
int parseBIThead(unsigned char *data, BIThead *head) {
	int boff = 0;

	memset(head, 0, sizeof(BIThead));

	head->table_id = getBit(data, &boff, 8);
	head->section_syntax_indicator = getBit(data, &boff, 1);
	head->reserved_future_use1 = getBit(data, &boff, 1);
	head->reserved1 = getBit(data, &boff, 2);
	head->section_length = getBit(data, &boff, 12);
	head->original_network_id = getBit(data, &boff, 16);
	head->reserved2 = getBit(data, &boff, 2);
	head->version_number = getBit(data, &boff, 5);
	head->current_next_indicator = getBit(data, &boff, 1);
	head->section_number = getBit(data, &boff, 8);
	head->last_section_number = getBit(data, &boff, 8);
	head->reserved_future_use2 = getBit(data, &boff, 3);
	head->broadcast_view_propriety = getBit(data, &boff, 1);
	head->first_descriptors_length = getBit(data, &boff, 12);
	return 10 + head->first_descriptors_length;
}

// 4-TR-B14v4_3-2p3.pdf p4-254 (276/543)
int parseBITloop(unsigned char *data, BITloop *loop) {
	int boff = 0;

	memset(loop, 0, sizeof(BITloop));

	loop->broadcaster_id = getBit(data, &boff, 8);
	loop->reserved_future_use = getBit(data, &boff, 4);
	loop->broadcaster_descriptors_length = getBit(data, &boff, 12);

	return 3;
}

// SI伝送パラメータ記述子
int parseBITdesc(unsigned char *data, BITdesc *desc) {
	int boff = 0;

	memset(desc, 0, sizeof(BITdesc));

	desc->descriptor_tag = getBit(data, &boff, 8);
	desc->descriptor_length = getBit(data, &boff, 8);
	desc->parameter_version = getBit(data, &boff, 8);
	desc->update_time = getBit(data, &boff,16);

	return 5;
}

int parseBITtable(unsigned char *data, BITtable *table) {
	int boff = 0;

	memset(table, 0, sizeof(BITtable));

	table->table_id = getBit(data, &boff, 8);
	table->table_description_length = getBit(data, &boff, 8);
	table->table_cycle = getBit(data, &boff, 16);

	return table->table_description_length + 2;
}

void dumpBIT(unsigned char *ptr)
{
	BIThead   bith;
	BITloop   bitl;
	BITdesc   bitd;
	BITtable  bitt;

	int len = 0;
	int loop_len = 0;
	int desc_len = 0;
	int table_len = 0;

	/* BIT */
	len = parseBIThead(ptr, &bith);
	ptr += len;
	loop_len = bith.section_length - (len - 3 + 4); // 3は共通ヘッダ長 4はCRC

	while(loop_len > 0) {
		len = parseBITloop(ptr, &bitl);
		ptr += len;
		loop_len -= len;

		desc_len = bitl.broadcaster_descriptors_length;
		loop_len -= desc_len;

		while(desc_len > 0) {
			len = parseBITdesc(ptr, &bitd);
			ptr += len;
			desc_len -= len;

			table_len = bitd.descriptor_length - 3;
			desc_len -= table_len;

			if ( bitd.descriptor_tag != 0xD7 ) {
				ptr += table_len;
				continue;
			}

			while(table_len > 0) {
				len = parseBITtable(ptr, &bitt);
				ptr += len;
				table_len -= len;

				/*
					0x4E (M-EIT,L-EIT)
					0x50(各局伝送パラメータ運用部分の H-EIT[schedule basic])
					0x58 (各局伝送パラメータ運用部分の H-EIT[schedule extended])
					0xC3(SDTT)
					0xC8(CDT)
				*/

				if (0) printf("BIT=(%d)%d:%d:%d:%d|0x%x:%d:%d=BCD(%d)\n",
					bitl.broadcaster_id, bitd.descriptor_tag, bitd.descriptor_length, 
					bitd.parameter_version, bitd.update_time, 
					bitt.table_id, bitt.table_description_length, 
					bitt.table_cycle, BCD(bitt.table_cycle));
			}
		}
	}

	return;
}

