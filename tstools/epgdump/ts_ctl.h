#ifndef	__TS_CONTROL_H__
#define	__TS_CONTROL_H__

#include	"util.h"

typedef	struct	_SVT_CONTROL	SVT_CONTROL;
struct	_SVT_CONTROL{
	SVT_CONTROL	*next ;
	SVT_CONTROL	*prev ;
	int		event_id ;			// イベントID
	int		original_network_id ;			// OriginalNetworkID
	int		transport_stream_id ;			// TransportStreamID
	char	servicename[MAXSECLEN] ;		// サービス名
};

typedef	struct	_EIT_CONTROL	EIT_CONTROL;
struct	_EIT_CONTROL{
	EIT_CONTROL	*next ;
	EIT_CONTROL	*prev ;
	int		table_id ;
	int		servid ;
	int		event_id ;			// イベントID
	int		content_type ;		// コンテントタイプ
	int		yy;
	int		mm;
	int		dd;
	int		hh;
	int		hm;
	int		ss;
	int		dhh;
	int		dhm;
	int		dss;
	int		ehh;
	int		emm;
	int		ess;
	char	*title ;			// タイトル
	char	*subtitle ;			// サブタイトル
	char	*desc ;				// Description
	int	desc_length ;			// Description Length
	int	video_type ;			// 映像のタイプ
	int	audio_type ;			// 音声のタイプ
	int	multi_type ;			// 音声の 2 カ国語多重
};

typedef	struct	_DSM_CONTROL	DSM_CONTROL;
struct	_DSM_CONTROL{
	int		isUsed ;
	int		moduleId ;
	int		lastBlockNumber ;
	int		blockSize ;
	void		*blockData ;
};

#endif

