#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import ConfigParser
import os
import os.path
mypath = str(os.path.dirname(os.path.abspath(__file__)))
confp = ConfigParser.SafeConfigParser()
Conf = 'rec10.conf'
confpath=os.path.join(mypath,Conf)
confp.read(confpath)
def getTempConfPath(string):
    global confp
    return confp.get('path', string)

def setTempConfPath(option,value):
    global confp
    return confp.set('path',option,value)

def setTempConfDB(option,value):
    global confp
    return confp.set('db',option,value)

def setTempConfEnv(option,value):
    global confp
    return confp.set('env',option,value)

def setTempConfDVB(option,value):
    global confp
    return confp.set('dvb',option,value)

def writeTempConf():
    global confp
    f=open(confpath,"w")
    confp.write(f)
    f.close
