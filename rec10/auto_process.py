#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import os
import glob
import time
import datetime
import commands
import re
import os.path

import chdb
import status
import configreader
import recdblist
import rec10d
import tv2mp4
import epgdb
import rec10const

def updateToMP4(path):
    """
    !現在未使用!
    録画一時フォルダ内mp4ファイルを検索
    """
    avilist = glob.glob(path + "/*.avi")
    mkvlist = glob.glob(path+"/*.mkv")
    avilist=avilist+mkvlist
    for avif in avilist:
        if avif.rfind("sa.avi")==-1:
            dir = os.path.split(avif)[0]
            title = os.path.split(avif)[1]
            title = title.replace(".avi", "")
            title = title.replace(".mkv", "")
            avipath = os.path.join(dir, title + ".avi")
            mkvpath = os.path.join(dir, title + ".mkv")
            mp4path = os.path.join(dir, title + ".mp4")
            if not os.path.exists(mp4path):
                if os.path.exists(avipath):
                    avidtime = int(time.time()-os.path.getmtime(avipath))
                    if avidtime>300:
                        if os.path.exists(mkvpath):
                            mkvdtime = int(time.time()-os.path.getmtime(mkvpath))
                            if mkvdtime>300:
                                recdblist.printutf8(mkvpath+":"+mp4path)
                                tv2mp4.mkv2mp4(mkvpath,mp4path)
                            else:
                                recdblist.printutf8(avipath+":"+mp4path)
                                tv2mp4.avi2mp4(avipath,mp4path)
                        else:
                            recdblist.printutf8(avipath+":"+mp4path)
                            tv2mp4.avi2mp4(avipath,mp4path)
                else:
                    if not os.path.exists(avipath):
                        if os.path.exists(mkvpath):
                            mkvdtime = int(time.time()-os.path.getmtime(mkvpath))
                            if mkvdtime>300:
                                recdblist.printutf8(mkvpath+":"+mp4path)
                                tv2mp4.mkv2mp4(mkvpath,mp4path)
                                
def autoCheck(path):
    avilist = glob.glob(path + "/*.avi")
    mkvlist = glob.glob(path+"/*.mkv")
    mp4list = glob.glob(path+"/*.mp4")
    tslist = glob.glob(path+"/*.ts")
    b25list = glob.glob(path+"/*.ts.b25")
    filelist=avilist+mkvlist+tslist+b25list+mp4list
    add=[]
    tbtime=datetime.datetime.now()+datetime.timedelta(seconds=60)
    tbtime2=datetime.datetime.now()+datetime.timedelta(seconds=1200)
    for fn in filelist:
        if os.path.exists(fn):
            if fn.rfind("sa.avi")==-1 and fn.rfind(".120.avi")==-1 and os.path.getsize(fn)>1*1000*1000:
                dir = os.path.split(fn)[0]
                file = os.path.split(fn)[1]
                title = os.path.splitext(file)[0]
                title = unicode(title,'utf-8',errors='replace')
                ext = os.path.splitext(file)[1]
                if ext == ".b25":
                    title=title.replace(".ts","")
                dbs=rec10d.rec10db.select_by_name_time_timeline(title,tbtime,tbtime2)
                if len(dbs)==0:
                    proc=checkProcess(dir, title)
                    chtxtt=rec10d.rec10db.select_chtxt_by_title_timeline_log(title)
                    nchtxt=""
                    if chtxtt!=None:
                        nchtxt=chtxtt
                    if proc=="b25":
                        add.append([rec10const.REC_AUTO_SUGGEST_DECODE,title,nchtxt])
                    elif proc=="ts":
                        add.append([rec10const.REC_AUTO_SUGGEST_ENCODE,title,nchtxt])
                    elif proc =="264":
                        add.append([rec10const.REC_AUTO_SUGGEST_AVI2FP,title,nchtxt])
                    #elif proc =="mp4":
                    #    add.append([rec10const.REC_AUTO_SUGGEST_AP2FP,title,nchtxt])
        #print add
    if len(add)>0:
        rec10d.rec10db.new_auto_proc()
        for a in add:
            rec10d.rec10db.add_auto_proc(a[0],a[1],a[2])
        time.sleep(1)

"""
    処理がどの段階まで言ったのかを調査し返す。
    return
    recording
    b25
    b25decoding
    tssplitting
    ts
    encoding
    avi
    mp4making
    mp4
"""
def checkProcess(path,title):
    path1 = os.path.join(path,title+".mkv")
    if os.path.exists(path1):
        if int(time.time()-os.path.getmtime(path1))>300:
            return "mkv"
        else:
            return "mkvmaking"
    elif os.path.exists(os.path.join(path,title+".mp4")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".mp4")))>300:
            return "mp4"
        else:
            return "mp4making"
    elif os.path.exists(os.path.join(path,title+".264")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".264")))>300:
            return "264"
        else:
            return "encoding"
    elif os.path.exists(os.path.join(path,title+".ts.log")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".ts")))<300:
            return "encoding"
        else:
            return "ts"
    elif os.path.exists(os.path.join(path,title+".ts")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".ts")))>300:
            return "ts"
        else:
            return "tssplitting"
    elif os.path.exists(os.path.join(path,title+".sa.avi")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".sa.avi")))>300:
            return "ts"
        else:
            return "tssplitting"
    elif os.path.exists(os.path.join(path,title+".ts.b25")):
        if int(time.time()-os.path.getmtime(os.path.join(path,title+".ts.b25")))>300:
            return "b25"
        else:
            return "recording"

def update_all_timeline_epg():
    now=datetime.datetime.now()
    et=now+datetime.timedelta(days=7)
    update_timeline_epg(now.strftime("%Y-%m-%d %H:%M:%S"), et.strftime("%Y-%m-%d %H:%M:%S"))

def update_timeline_epg(btime,etime):
    update_timeline_epg_schedule(btime,etime)
    update_timeline_dup(btime,etime)

def update_timeline_epg_schedule(btime,etime):
    dbl=rec10d.rec10db.select_bytime_all_timeline(btime, etime)
    #print dbl
    for db in dbl:
        ret=[]
        #[chtxtt, title, btime, etime,exp,longexp,category]
        if db['type']==rec10const.REC_KEYWORD or db['type']==rec10const.REC_KEYWORD_EVERY_SOME_DAYS:
            ret=epgdb.searchTime(db['title'], db['btime'], db['deltatime'], db['chtxt'])
            if len(ret)>4 and len(ret[2])>18:
                rec10d.rec10db.update_epg_timeline(db['type'], db['chtxt'], db['title'], db['btime'], ret[2],ret[3],ret[1],ret[4],ret[6])
                if not (db['btime'] == ret[2] and  db['etime']==ret[3]):
                    rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")
                else:
                    rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "0")
            else:
                rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")
        elif db['type']==rec10const.REC_RESERVE or db['type']==rec10const.REC_FINAL_RESERVE :
            ret=epgdb.searchTime(db['title'], db['btime'],"5", db['chtxt'])
            if len(ret)>4 and len(ret[2])>18:
                rec10d.rec10db.update_epg_timeline(db['type'], db['chtxt'], db['title'], db['btime'], ret[2],ret[3],ret[1],ret[4],ret[6])
                if not (db['btime'] == ret[2] and  db['etime']==ret[3]):
                    rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")
                else:
                    rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "0")
            else:
                rec10d.rec10db.update_status_change_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")

def update_timeline_dup(epgbtime,epgetime):
    dbl=rec10d.rec10db.select_byepgtime_all_timeline(epgbtime, epgetime)
    for db in dbl:
        if db['type']==rec10const.REC_KEYWORD or db['type']==rec10const.REC_KEYWORD_EVERY_SOME_DAYS:
            dbn=epgdb.countEpgSchedule(db['epgbtime'], db['epgetime'])
            try:
                bctypet=chdb.searchCHFromChtxt(db['chtxt'])['bctype']
                if bctypet.find("cs") > -1 or bctypet.find("bs") > -1 :
                    if dbn[1]>status.getRecordingMax()[1]:
                        rec10d.rec10db.update_status_dup_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")
                    else:
                        rec10d.rec10db.update_status_dup_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "0")
                else:
                    if dbn[0]>status.getRecordingMax()[0]:
                        rec10d.rec10db.update_status_dup_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "1")
                    else:
                        rec10d.rec10db.update_status_dup_timeline(db['type'], db['chtxt'], db['title'], db['btime'], "0")
            except:
                ""

def killDeadEncode(path):
    rawlist = glob.glob(path + "/*.264")
    for fn in rawlist:
        if int(time.time()-os.path.getmtime(fn))>3000:
            m2vpath=unicode(fn.replace(".264",".m2v"),'utf-8')
            x264path=os.path.split(fn)[1]
            folderpath=unicode(os.path.split(fn)[0],'utf-8')
            x264path=re.sub("\\[","[",x264path)
            x264path=re.sub("\[","\\\[",x264path)
            x264path=re.sub("\\]","]",x264path)
            x264path=re.sub("]","\\]",x264path)
            tspath=unicode(x264path.replace(".264",".ts"),'utf-8')
            m2vpath=unicode(x264path.replace(".264",".m2v"),'utf-8')
            x264path=unicode(x264path,'utf-8')
            os.environ['LANG']="ja_JP.UTF-8"
            ktmp=[]
            ktmp.append(u'ps auxww | grep "%(folderpath)s" | grep "%(x264path)s" | egrep -v grep | egrep -v "sh -c" | grep x264 | awk \'{print $2}\'' %locals())
            ktmp.append(u'ps auxww | grep "%(folderpath)s" | grep "%(tspath)s" | egrep -v grep | egrep -v "sh -c" | grep mencoder | awk \'{print $2}\''%locals())
            ktmp.append(u'ps auxww | grep "%(folderpath)s" | grep "%(m2vpath)s" | egrep -v grep | egrep -v "sh -c" | grep ffmpeg | awk \'{print $2}\''%locals())
            recdblist.printutf8(u"エンコード処理異常終了タスク終了")
            for istr in ktmp:
                recdblist.printutf8(istr)
                ktmt=commands.getoutput(istr.encode('utf-8'))
                if len(ktmt)>0:
                    if len(ktmt[0])>0:
                        ktmt=ktmt[0]
                try:
                    if int(ktmt)>0:
                        ktmp=u"kill -9 `"+istr+u"`"
                        recdblist.printutf8(ktmp)
                        #print ktmp
                        os.system(ktmp.encode('utf-8'))
                except:
                    ""

def deleteTmpFile(path,title,ext):
    level= 0#0:b25 5:ts(del tsmix and ts.b25) 9:x264(del 2 and so on) 10:mp4/mkv
    smsize= 0
    if re.search(".ts",ext):
        level= 5
        smsize = 100*1000*1000
    elif re.search(".264",ext):
        level= 9
        smsize = 10*1000*1000
    elif re.search(".mp4",ext):
        level = 10
        smsize = 10*1000*1000
    elif re.search(".mkv",ext):
        level = 15
        smsize = 10*1000*1000
    dp=[]
    if level >= 1 :
        if os.path.exists(os.path.join(path,title+".ts")) and os.path.exists(os.path.join(path,title+".ts.b25")):
            if os.path.getsize(os.path.join(path,title+".ts"))*12>os.path.getsize(os.path.join(path,title+".ts.b25")):
                for pext in rec10const.EXT_LEVEL_0:
                    dp.append(os.path.join(path,title+pext))
            elif os.path.getsize(os.path.join(path,title+".ts"))>200*1000*1000:
                for pext in rec10const.EXT_LEVEL_0:
                    dp.append(os.path.join(path,title+pext))
    if level >= 5 :
        for pext in rec10const.EXT_LEVEL_4:
            dp.append(os.path.join(path,title+pext))
    if level >= 10 :
        if configreader.getConfEnv("remove_ts")=="1":
            dp.append(os.path.join(path,title+".ts"))
        for pext in rec10const.EXT_LEVEL_9:
            dp.append(os.path.join(path,title+pext))
    if os.path.exists(os.path.join(path,title+ext)):
        if os.path.getsize(os.path.join(path,title+ext))>smsize:
            for ip in dp:
                try:
                    os.remove(ip)
                except:
                    ""
