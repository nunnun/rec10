#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2011 Yukikaze
import os
import sys
import time

import chdb
import epgdb
import recdblist

update = chdb.update()
if len(update) > 0:
    #recdblist.printutf8(u"番組表を更新")
    #print update
    pid = os.fork()
    if pid > 0:#親プロセスの場合
        ""
    else:#子プロセスの場合　アップデートを行って終了
        for bctype in update:
            time.sleep(1)
            epgdb.updatebc(bctype)
        sys.exit(0)