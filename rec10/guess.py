#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2011 Yukikaze

import os
import os.path
import re
import time
import os
import datetime
import n_gram
import recdblist
import zenhan
import auto_move

def detSeriesNum(execpath):
    files=os.listdir(execpath)
    ss={}
    for file in files:
        ftitle=os.path.splitext(file)
        fname=ftitle[0]
        fname=zenhan.toHankaku(unicode(fname,"utf-8"))
        nt=detNameType(fname, execpath)
        st=nt['title']
        if not ss.has_key(st):
            ss[st]={}
        ss[st][nt['num']]=ftitle
        print ss
    return ss
def detDeltaNum(title,movepath):
    """
        番組の話数を推測する。
    """
    files=os.listdir(movepath)
    ff=[]
    maxnum=0
    for file in files:
        recdblist.printutf8(unicode(file,'utf-8'),verbose_level=800)
        file=os.path.join(movepath,file)
        if os.path.isfile(file):
            name=os.path.splitext(os.path.split(file)[1])
            name=name[0]
            name=zenhan.toHankaku(unicode(name,'utf-8'))
            p1=detNameType(name,movepath)
            #recdblist.printutf8(p1['title']+" "+str(p1['num']))
            time1=time.localtime(os.path.getmtime(file))
            time1=datetime.datetime.fromtimestamp(os.path.getmtime(file))
            if p1['num']!=-1:
                ff.append([p1['num'],p1['title'],time1])
                if maxnum<p1['num']+1:
                    maxnum=p1['num']+1
    return detMultiDeltaDays(ff)

def detMultiDeltaDays(num_with_title_with_dates):
    maxnum=0
    for ft in num_with_title_with_dates:
        if maxnum<ft[0]+1:
            maxnum=ft[0]+1
    ff=num_with_title_with_dates
    f3=[None]*maxnum
    fret={}
    for i in range(0, maxnum, 1):
        f3[i]=[]
    for f2 in ff:
        f3[f2[0]].append(f2)
    for i in range(maxnum):
        for j in range(i+1,maxnum,1):
            for ft3 in f3[i]:
                for ft4 in f3[j]:
                    for gdd in detSingleDeltaDay(i, ft3[2], j, ft4[2]):
                        #print gdd
                        if fret.get(gdd[0])!=None:
                            #print fret
                            fret[gdd[0]]=fret[gdd[0]]+gdd[1]
                        else:
                            fret[gdd[0]]=gdd[1]
    maxk=0
    maxp=0
    for i in range(maxnum):
        if fret.get(i)!=None:
            if maxp<fret[i]:
                maxk=i
                maxp=fret[i]
    return maxk

def detSingleDeltaDay(num1,date1,num2,date2):
    dd=date1-date2
    if date1<date2:
        dd=date2-date1
    dn=dd.days
    d=num2-num1
    if d<0:
        d=-1*d
    dp=dd+datetime.timedelta(hours=6)
    dp=dp.days
    dm=dd-datetime.timedelta(hours=6)
    dm=dm.days
    ret=[]
    if dn%d*2>d:
        dn=dn+d
    if dm%d*2>d:
        dm=dm+d
    if dp%d*2>d:
        dp=dp+d
    #recdblist.printutf8(str(d)+":"+str(dn))
    if dp!=dn:
        ret.append([dn/d,60])
        ret.append([dp/d,40])
    elif dm!=dn:
        ret.append([dn/d,60])
        ret.append([dm/d,40])
    else:
        ret.append([dn/d,100])
    return ret

def detNameType(title,path):
    """
    type A ---title#<number>
    type B ---title#<number>subtitle
    type C ---title subtitle
    type D ---title(without number)
    type Aj ---title第<number>話
    path --search reflexively
    """
    new=0
    if re.search(u"[新]",title) or re.search(u" 新",title):
        title=title.replace(u"[新]","")
        title=title.replace(u" 新","")
        new=1
    recdblist.printutf8(title,verbose_level=800)
    title=auto_move.getTitle(title)##titleから日時を除く
    title=title.replace(u"無料≫","")
    #rA=re.compile(".+(?P<title>)#\d(?P<num>)\s[0,10]\z")
    rA=re.compile("(.+)#(\d*)\s*\Z")
    tA=rA.match(title)
    rB=re.compile("(.+)#(\d*)\s*(\D*)")
    tB=rB.match(title)
    rAj=re.compile("(.+)第(\d*)話\s*\Z")
    tAj=rAj.match(title)
    ret={'title':"",'type':"",'num':0,'subtitle':"",'folder':""}
    if tA:
        #recdblist.printutf8("typeA")
        #recdblist.printutf8("title="+tA.group(1))
        #recdblist.printutf8("num="+tA.group(2))
        ret['type']="A"
        ret['title']=tA.group(1).replace(" ","")
        ret['num']=int(tA.group(2))
        ret['folder']=searchFolder(tA.group(1),unicode(path,'utf-8'))
    if tAj:
        #recdblist.printutf8("typeA")
        #recdblist.printutf8("title="+tAj.group(1))
        #recdblist.printutf8("num="+tAj.group(2))
        ret['type']="Aj"
        ret['title']=tAj.group(1).replace(" ","")
        ret['num']=int(tAj.group(2))
        ret['folder']=searchFolder(tAj.group(1),unicode(path,'utf-8'))
    elif tB:
        #recdblist.printutf8("typeB")
        #recdblist.printutf8("title="+tB.group(1))
        #recdblist.printutf8("num="+tB.group(2))
        #recdblist.printutf8("subtitle="+tB.group(3))
        ret['type']="B"
        ret['title']=tB.group(1).replace(" ","")
        ret['num']=int(tB.group(2))
        ret['folder']=searchFolder(tB.group(1),unicode(path,'utf-8'))
        ret['subtitle']=tB.group(3)
    else:#type C or type D
        #fold=searchFolder(title, path)
        ts=title.split(" ")
        tt=""
        rt=["",0,""]
        for t in ts:
            tt=tt+" "+t
            ft1=searchFolder(tt,unicode(path,'utf-8'))
            #recdblist.printutf8(tt)
            #print ft1
            if ft1!="":
                #recdblist.printutf8(rt)
                #recdblist.printutf8(ft1[0]+" : "+str(ft1[1]))
                if ft1[1]>rt[1]:
                    rt[0]=tt
                    rt[1]=ft1[1]
                    rt[2]=ft1[0]
                    #recdblist.printutf8(rt)
        #recdblist.printutf8("title="+rt[0][1:]+"/")
        #recdblist.printutf8("subtitle = "+title.replace(rt[0][1:],"")[1:])
        ret['title']=rt[0][1:].replace(" ","")
        ret['num']=-1
        ret['folder']=rt[2]
        ret['subtitle']=title.replace(rt[0][1:],"")[1:]
        if ret['subtitle'].replace(" ","")=="":
            ret['type']="D"
        else:
            ret['type']="C"
    if new==1:
        ret['num']=1
    return ret

def searchFolder(title,path,threshold=500):
    """
    titleからフォルダーを探す
    """
    folderpath=os.listdir(path)
    lfpath=[]
    ngram=[]
    for ft in folderpath:
        fullpath=os.path.join(path, ft)
        if os.path.isdir(fullpath):
            lfpath.append(fullpath)
            ftt=os.listdir(fullpath)
            if len(ftt)>0:
                for ft2 in ftt:
                    folderpath.append(os.path.join(fullpath, ft2))
        else:
            lfpath.append(fullpath)
    for dirp in lfpath:
        cmpp=""
        appp=""
        if os.path.isdir(dirp):
            cmpp=os.path.dirname(dirp)
            appp=dirp
        else:
            cmpp=os.path.basename(dirp)
            appp=os.path.dirname(dirp)
        ntitle=auto_move.getTitle(title)
        ncmpp=auto_move.getTitle(cmpp)
        p=n_gram.trigram(ntitle,ncmpp)
        if p>0:
            ngram.append((p,appp))
    ngram=list(set(ngram))
    ngram.sort()
    ngram.reverse()
    if len(ngram)>0:
        #recdblist.printutf8(title + ngram[0][1] + " : "+str(ngram[0][0]))
        if ngram[0][0]>threshold:
            return ngram[0][1]
        else:
            return ""
    else:
        return ""