#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2011 Yukikaze
import datetime
import os
import shutil
import time
import os.path
import traceback

import chdb
import n_gram
import rec10d
import ts2epg
import xml2db_dom
import configreader
import recdblist
path = str(os.path.dirname(os.path.abspath(__file__))) + "/"
tmppath = configreader.getConfPath("tmp")+"/"
if tmppath=="/":
    tmppath=path
if not os.path.exists(tmppath):
    os.mkdir(tmppath)
def update(chtxt):
    recdblist.printutf8(chtxt)
    tnow = datetime.datetime.now()
    try:
        lastup = datetime.datetime.strptime(chdb.searchCHFromChtxt(chtxt)[0]['update'], "%Y-%m-%d %H:%M:%S")
    except:
        lastup = datetime.datetime.strptime("2009-04-01 00:00:00", "%Y-%m-%d %H:%M:%S")
    dt = tnow-lastup
    dt = dt.days * 24 * 60 + dt.seconds
    if dt > 2 * 60 * 60:
        ts2epg.writeEpgXML(os.path.join(tmppath, chtxt + "epgdata.xml"), chdb.searchCHFromChtxt(chtxt)['ch'])
        xml2db_dom.xml2db_dom(os.path.join(tmppath, chtxt + "epgdata.xml"), chdb.searchCHFromChtxt(chtxt)['bctype'])
def updatebc(bctype):
    bctypel = chdb.searchCHFromBctype(bctype)
    #print bctype
    try:
        ts2epg.writeEpgXML(tmppath + bctype + "epgdata.xml", chdb.searchCHFromBctype(bctype)['ch'])
        xml2db_dom.xml2db_dom(tmppath + bctype + "epgdata.xml", bctype)
        shutil.copyfile(tmppath + bctype + "epgdata.xml", tmppath + bctype + "epgdata.bak.xml")
        time.sleep(5)
        rec10d.rec10db.update_by_bctype_epg_ch(bctype)
    except Exception, inst:
        recdblist.addCommonlogEX("Error","updatebc(epgdb.py)", str(type(inst)), str(inst)+"\n"+traceback.format_exc(),log_level=200)
        xml2db_dom.xml2db_dom(tmppath + bctype + "epgdata.bak.xml", bctype)
        rec10d.rec10db.update_status_by_bctype_epg_ch(bctype, "2")
def updateLogo_bc(bctype):
    bctypel = chdb.searchCHFromBctype(bctype)
    #print bctype
    try:
        ts2epg.writeLogoEpgXML(tmppath + bctype + "logo.xml", chdb.searchCHFromBctype(bctype)['ch'])
        xml2db_dom.xml2db_dom_logo(tmppath + bctype + "logo.xml", bctype)
        shutil.copyfile(tmppath + bctype + "logo.xml", tmppath + bctype + "logo.bak.xml")
        time.sleep(5)
        rec10d.rec10db.update_logostatus_by_bctype_epg_ch(bctype,"1")
    except Exception, inst:
        recdblist.addCommonlogEX("Error","updatebc(epgdb.py)", str(type(inst)), str(inst)+"\n"+traceback.format_exc(),log_level=200)
        xml2db_dom.xml2db_dom(tmppath + bctype + "logo.bak.xml", bctype)
        rec10d.rec10db.update_logostatus_by_bctype_epg_ch(bctype,"2")
def searchTime(titletxt, timet, deltatime, chtxt):
    time = datetime.datetime.strptime(timet, "%Y-%m-%d %H:%M:%S")
    ret = []
    deltatime = int(deltatime)
    beforetime = time.strftime("%Y%m%d%H%M%S")
    beforetime = time - datetime.timedelta(hours=deltatime)
    aftertime = time + datetime.timedelta(hours=deltatime)
    beforetime = beforetime.strftime("%Y%m%d%H%M%S")
    aftertime = aftertime.strftime("%Y%m%d%H%M%S")
    one = ["", "", "", "","","",""]
    tempt = 0
    chtxtt=chtxt.replace("_0","_%")
    d = rec10d.rec10db.select_by_time_ngram_epg_timeline(beforetime, aftertime, chtxtt)
    for chtxtt, title, start, stop ,exp,longexp,category in d:
        #recdblist.printutf8(chtxtt+":"+title+":"+start+":"+stop)
        p = n_gram.bigram(titletxt, title)
        btime = start
        btime = btime[0:4] + "-" + btime[4:6] + "-" + btime[6:8] + " " + btime[8:10] + ":" + btime[10:12] + ":00"
        etime = stop
        etime = etime[0:4] + "-" + etime[4:6] + "-" + etime[6:8] + " " + etime[8:10] + ":" + etime[10:12] + ":00"
        bt = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
        dt = bt-time
        dt = abs(dt)
        dt = 24 * 60 * 60 * dt.days + dt.seconds + 1
        p = p * 90
        dt = 1000-1000 * dt / (7 * 24 * 60 * 60)
        if p > 0:
            p = p + dt
        if p > tempt:
            one = [chtxtt, title, btime, etime,exp,longexp,category]
            tempt = p
        if p > 0:
            #recdblist.printutf8(title + ":" + str(p) + u"点")
            ""
    ret = one
    #print ret
    return ret
def searchTimeAuto(keytxt, timet, deltatime):
    time = datetime.datetime.strptime(timet, "%Y-%m-%d %H:%M:%S")
    ret = []
    deltatime = int(deltatime)
    beforetime = time.strftime("%Y%m%d%H%M%S")
    aftertime = time + datetime.timedelta(hours=deltatime)
    aftertime = aftertime.strftime("%Y%m%d%H%M%S")
    d = rec10d.rec10db.select_by_time_keyword_auto_suggest_epg_timeline(keytxt,beforetime, aftertime)
    for chtxtt, title, start, stop ,exp,longexp,category in d:
        btime = start
        btime = btime[0:4] + "-" + btime[4:6] + "-" + btime[6:8] + " " + btime[8:10] + ":" + btime[10:12] + ":00"
        etime = stop
        etime = etime[0:4] + "-" + etime[4:6] + "-" + etime[6:8] + " " + etime[8:10] + ":" + etime[10:12] + ":00"
        ret.append([chtxtt, title, btime, etime,exp,longexp,category])
    return ret
def countSchedule(btime,etime):
    """
    return rec num (int return[0]:TE /int return[1]:BS/CS)
    """
    def cmpare(x, y):
        xt = x
        yt = y
        if xt > yt:
            return -1
        elif xt == yt:
            return 0
        else:
            return 1
    dls = rec10d.rec10db.select_bytime_all_timeline(btime, etime)
    times = []
    btd = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
    etd = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
    for dl in dls:
        dd1 = datetime.datetime.strptime(dl['btime'], "%Y-%m-%d %H:%M:%S")
        dd2 = datetime.datetime.strptime(dl['etime'], "%Y-%m-%d %H:%M:%S")
        if dd1 < btd:
            dd1 = btd
        if dd2 > etd:
            dd2 = etd
        times.append(dd1)
        times.append(dd2)
    times = list(set(times))
    times.sort(cmpare)
    times.reverse()
    retcount = [0, 0]
    for i in xrange(0, len(times)-2, 1):
        bt1 = times[i]
        et1 = times[i + 1]
        btime1 = bt1.strftime("%Y-%m-%d %H:%M:%S")
        etime1 = et1.strftime("%Y-%m-%d %H:%M:%S")
        ret1 = rec10d.rec10db.count_schedule_timeline(btime1, etime1)
        if retcount[0] < ret1[0]:
            retcount[0] = ret1[0]
        if retcount[1] < ret1[1]:
            retcount[1] = ret1[1]
    return retcount

def searchSchedule(btime,etime):
    """
    return rec num (int return[0]:TE /int return[1]:BS/CS)
    """
    def cmpare(x, y):
        xt = x
        yt = y
        if xt > yt:
            return -1
        elif xt == yt:
            return 0
        else:
            return 1
    dls = rec10d.rec10db.select_bytime_all_timeline(btime, etime)
    times = []
    btd = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
    etd = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
    for dl in dls:
        dd1 = datetime.datetime.strptime(dl['btime'], "%Y-%m-%d %H:%M:%S")
        dd2 = datetime.datetime.strptime(dl['etime'], "%Y-%m-%d %H:%M:%S")
        if dd1 < btd:
            dd1 = btd
        if dd2 > etd:
            dd2 = etd
        times.append(dd1)
        times.append(dd2)
    times = list(set(times))
    times.sort(cmpare)
    times.reverse()
    ret=[]
    for i in xrange(0, len(times)-2, 1):
        bt1 = times[i]
        et1 = times[i + 1]
        btime1 = bt1.strftime("%Y-%m-%d %H:%M:%S")
        etime1 = et1.strftime("%Y-%m-%d %H:%M:%S")
        ret1 = rec10d.rec10db.search_schedule_timeline(btime1, etime1)
        ret.append(ret1)
    ret = list(set(ret))
    return ret

def countEpgSchedule(epgbtime,epgetime):
    """
    return rec num (int return[0]:TE /int return[1]:BS/CS)
    """
    def cmpare(x, y):
        xt = x
        yt = y
        if xt > yt:
            return -1
        elif xt == yt:
            return 0
        else:
            return 1
    dls = rec10d.rec10db.select_byepgtime_over_timeline(epgbtime, epgetime)
    #print dls
    times = []
    btd = datetime.datetime.strptime(epgbtime, "%Y-%m-%d %H:%M:%S")
    etd = datetime.datetime.strptime(epgetime, "%Y-%m-%d %H:%M:%S")
    for dl in dls:
        dd1 = datetime.datetime.strptime(dl['epgbtime'], "%Y-%m-%d %H:%M:%S")
        dd2 = datetime.datetime.strptime(dl['epgetime'], "%Y-%m-%d %H:%M:%S")
        if dd1 < btd:
            dd1 = btd
        if dd2 > etd:
            dd2 = etd
        times.append(dd1)
        times.append(dd2)
    times = list(set(times))
    times.sort(cmpare)
    times.reverse()
    retcount = [0, 0]
    for i in xrange(0, len(times)-1, 1):
        bt1 = times[i]
        et1 = times[i + 1]
        epgbtime1 = bt1.strftime("%Y-%m-%d %H:%M:%S")
        epgetime1 = et1.strftime("%Y-%m-%d %H:%M:%S")
        ret1 = rec10d.rec10db.count_epgschedule_timeline(epgbtime1, epgetime1)
        if retcount[0] < ret1[0]:
            retcount[0] = ret1[0]
        if retcount[1] < ret1[1]:
            retcount[1] = ret1[1]
    return retcount
def searchEpgSchedule(epgbtime,epgetime):
    """
    return rec num (int return[0]:TE /int return[1]:BS/CS)
    """
    def cmpare(x, y):
        xt = x
        yt = y
        if xt > yt:
            return -1
        elif xt == yt:
            return 0
        else:
            return 1
    dls = rec10d.rec10db.select_byepgtime_all_timeline(epgbtime, epgetime)
    times = []
    btd = datetime.datetime.strptime(epgbtime, "%Y-%m-%d %H:%M:%S")
    etd = datetime.datetime.strptime(epgetime, "%Y-%m-%d %H:%M:%S")
    for dl in dls:
        dd1 = datetime.datetime.strptime(dl['epgbtime'], "%Y-%m-%d %H:%M:%S")
        dd2 = datetime.datetime.strptime(dl['epgetime'], "%Y-%m-%d %H:%M:%S")
        if dd1 < btd:
            dd1 = btd
        if dd2 > etd:
            dd2 = etd
        times.append(dd1)
        times.append(dd2)
    times = list(set(times))
    times.sort(cmpare)
    times.reverse()
    ret=[]
    for i in xrange(0, len(times)-2, 1):
        bt1 = times[i]
        et1 = times[i + 1]
        epgbtime1 = bt1.strftime("%Y-%m-%d %H:%M:%S")
        epgetime1 = et1.strftime("%Y-%m-%d %H:%M:%S")
        ret1 = rec10d.rec10db.search_epgschedule_timeline(epgbtime1, epgetime1)
        ret.append(ret1)
    ret = list(set(ret))
    return ret
