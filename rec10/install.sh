#!/bin/bash
SELF_DIR=`dirname $0`
cd ${SELF_DIR}
if type -P python2.6 > /dev/null
then export LANG="ja_JP.UTF-8" && python2.6 ./install.py
elif type -P python26 > /dev/null
then export LANG="ja_JP.UTF-8" && python26 ./install.py
elif type -P python2.7 > /dev/null
then export LANG="ja_JP.UTF-8" && python2.7 ./install.py
elif type -P python27 > /dev/null
then export LANG="ja_JP.UTF-8" && python27 ./install.py
elif type -P python2.5 > /dev/null
then export LANG="ja_JP.UTF-8" && python2.5 ./install.py
elif type -P python25 > /dev/null
then export LANG="ja_JP.UTF-8" && python25 ./install.py
fi
cp Makefile.base Makefile
