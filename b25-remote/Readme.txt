
− b25-remote Ver.0.0.1 −


１．概要
　　・スマートカードリーダをLANを経由で共有するためのシステムです。
　　・BonCasLink互換プロトコルを使用します。

　
２．注意事項
　　・一般的にスマートカードには個人を特定可能な情報が含まれている可能性があります。
　　・本ソフトウェアは通信の暗号化を行わないためこのリスクを十分に考えた上でご使用ください。
　　・本ソフトウェアの警告を無視しLAN以外のネットワークにおいて使用した結果発生したいかなる
　　　損害も作者に責任を求めないこととします。


３．ライセンスについて
　　・本パッケージに含まれる全てのソースコード、バイナリについて著作権は一切主張しません。
　　・オリジナルのまま又は改変し、各自のソフトウェアに自由に添付、組み込むことができます。
　　・但しGPLに従うことを要求しますのでこれらを行う場合はソースコードの開示が必須となります。
　　・このとき本ソフトウェアの著作権表示を行うかどうかは任意です。

　　・通信プロトコルの変更及び追加を含むソースコードの改変は一切許可できません。
　　・上記を禁止するいかなる根拠または拘束力も作者にはありませんがこれに反して、
　　　作成した改変物を配布する場合は下記を要求します。
　　・ソフトウェアの名称を「b25-remote」以外に変更すること。
　　・「Rec10」の著作権表示を一切行わないこと。
　　・ソースコードの流用元の表示を一切行わないこと。
　　・このソースコードは拡張ツール中の人(nakanohito@2sen.dip.jp)が作成されたBonCasLink(公式サイト:http://2sen.dip.jp/friio/)のソースコードを流用しています。

　　・ビルドに必要な環境
　　　- pcsc-lite-devel　※Fedora系
　　　- libpcsclite-dev　※Debian系


４．使用方法
　　①スマートカードリーダが接続されたPCで「b25-server」を起動します。

　　②LANに参加している他のPCでPCSCを利用するアプリケーションを起動します。

　　③このとき環境変数を以下のように設定します。
　　　LD_PRELOAD      = {b25-client.soの絶対パス}
　　　B25_SERVER_IP   = {サーバーのアドレス(DNS可)}
　　　B25_SERVER_PORT = {サーバーの使用ポート(デフォルト:6900)}

　　　※ファイヤウォールやフィルタリングを使用している場合は共有に使用するポートを開放してください。


６．サポート、連絡先
　　　連絡先　　：　Rec10
　　　公式サイト：　http://sourceforge.jp/projects/rec10/


８．更新履歴
　Ver.0.0.1 ・初回リリース

